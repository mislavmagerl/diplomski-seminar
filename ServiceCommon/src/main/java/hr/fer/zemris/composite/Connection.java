package hr.fer.zemris.composite;

import hr.fer.zemris.composite.service.MultiplyService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class Connection {

    public static MultiplyService multiplyService() {
        Service service = getService(
                MultiplyService.HOST,
                MultiplyService.PORT,
                MultiplyService.NAME
        );
        return service.getPort(MultiplyService.class);
    }

    public static String generateAddress(String host, int port, String name){
        return "http://" + host + ":" + port + "/ws/" + name;
    }

    public static Service getService(String host, int port, String name){
        URL wsdlURL = getWsdlURL(host, port, name);
        QName qname = getQName(name);

        return Service.create(wsdlURL, qname);
    }

    private static URL getWsdlURL(String host, int port, String name){
        URL wsdlURL = null;
        try {
            wsdlURL = new URL(generateAddress(host, port, name) + "?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return wsdlURL;
    }

    private static QName getQName(String serviceName){
        return new QName(Constants.NAMESPACE_URI, serviceName + "ImplService");
    }

}
