package hr.fer.zemris.composite.service;

import hr.fer.zemris.composite.model.Matrix;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface MultiplyService {

    String NAME = "MultiplyService";

    Integer PORT = 9999;

    String HOST = "localhost";

    @WebMethod
    /**
     * Returns the result of multiplication of matrices.
     * @param m1 first matrix
     * @param m2 second matrix
     * @return first matrix multiplied by the second matrix
     */
    Matrix multiply(Matrix m1, Matrix m2);

}
