package hr.fer.zemris.composite.model;

import java.util.Arrays;

public class Matrix {

    private int rowsCount;

    private int colsCount;

    private double[][] elements;

    public Matrix() {
    }

    public Matrix(int dimension) {
        this(dimension, dimension);
    }

    public Matrix(int rowsCount, int colsCount) {
        this(rowsCount, colsCount, new double[rowsCount][colsCount]);
    }

    public Matrix(int rowsCount, int colsCount, double[][] elements) {
        this.rowsCount = rowsCount;
        this.colsCount = colsCount;
        this.elements = elements;
    }

    public int getRowsCount() {
        return rowsCount;
    }

    public void setRowsCount(int rowsCount) {
        this.rowsCount = rowsCount;
    }

    public int getColsCount() {
        return colsCount;
    }

    public void setColsCount(int colsCount) {
        this.colsCount = colsCount;
    }

    public double[][] getElements() {
        return elements;
    }

    public void setElements(double[][] elements) {
        this.elements = elements;
    }

    public double getElement(int row, int col) {
        checkPositionIndex(row, col);
        return elements[row][col];
    }

    public Matrix set(int row, int col, double value) {
        checkPositionIndex(row, col);
        elements[row][col] = value;
        return this;
    }

    public Matrix newInstance(int rows, int cols) {
        return new Matrix(rows, cols);
    }

    public double[][] toArray() {
        double[][] array = new double[rowsCount][colsCount];
        for (int row = 0; row < rowsCount; row++) {
            array[row] = Arrays.copyOf(elements[row], elements[row].length);
        }
        return array;
    }

    /**
     * Metoda za parsiranje stringa u matricu. Format unosa je 1 2 3 | 2 3 4
     *
     * @param string String kojeg parsamo
     * @return matrica dane vrijednosti
     */
    public static Matrix parse(String string) {
        if (string == null) {
            throw new IllegalArgumentException("Dobio sam null za String");
        }
        String[] vectors = string.trim().split("\\|");

        int rows = vectors.length;
        int cols = vectors[0].trim().split("\\s+").length;
        double[][] elements = new double[rows][cols];

        for (int i = 0; i < vectors.length; i++) {
            String[] values = vectors[i].trim().split("\\s+");
            for (int j = 0; j < values.length; j++) {
                elements[i][j] = Double.parseDouble(values[j].trim());
            }
        }
        return new Matrix(rows, cols, elements);
    }

    public String toString(int precision) {
        StringBuilder sb = new StringBuilder();

        sb.append("redova: " + this.getRowsCount() + "\n");
        sb.append("stupaca: " + this.getColsCount() + "\n");

        int rows = this.getRowsCount();
        int cols = this.getColsCount();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                sb.append(String.format("%" + (3 + precision) + "." + precision + "f  ", getElement(i, j)));
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return toString(3);
    }

    private void checkPositionIndex(int row, int col) {
        if (row < 0 || col < 0 || row >= rowsCount || col >= colsCount) {
            throw new IllegalArgumentException("Illegal position: " + row + ", " + col + ". Matrix dimensions are " + rowsCount + " x " + colsCount);
        }
    }
}
