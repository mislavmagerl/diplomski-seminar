package hr.fer.zemris.composite;

import hr.fer.zemris.composite.model.Matrix;
import hr.fer.zemris.composite.service.MultiplyService;

import java.net.MalformedURLException;

public class Client {

    public static void main(String[] args) throws MalformedURLException {
        MultiplyService multiplyService = Connection.multiplyService();

        Matrix m1= Matrix.parse("0 0 0 0.5 | 0.5 0 0.5 0.5 | 0 1 0 0 | 0.5 0 0.5 0");

        Matrix m2 = multiplyService.multiply(m1, m1);
        Matrix m3 = multiplyService.multiply(m2, m1);

        Matrix r = Matrix.parse("0.25 | 0.25 | 0.25 | 0.25");

        Matrix result = multiplyService.multiply(m3, r);
        System.out.println(result);

    }

}