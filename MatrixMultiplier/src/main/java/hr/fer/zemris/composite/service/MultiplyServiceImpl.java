package hr.fer.zemris.composite.service;

import hr.fer.zemris.composite.model.Matrix;

import javax.jws.WebService;

@WebService(endpointInterface = "hr.fer.zemris.composite.service.MultiplyService")
public class MultiplyServiceImpl implements MultiplyService {

    public Matrix multiply(Matrix m1, Matrix m2) {

        System.out.println(m1);
        System.out.println("puta\n");
        System.out.println(m2);

        if (m1.getColsCount() != m2.getRowsCount()) {
            throw new IllegalArgumentException("Broj redaka prve matrice mora biti broju stupaca druge matrice");
        }
        int rows = m1.getRowsCount();
        int cols = m2.getColsCount();
        int others = m1.getColsCount();
        Matrix matrix = new Matrix(rows, cols);
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                double value = 0;
                for (int k = 0; k < others; k++) {
                    value += m1.getElement(row, k) * m2.getElement(k, col);
                }
                matrix.set(row, col, value);
            }
        }
        return matrix;
    }
}
