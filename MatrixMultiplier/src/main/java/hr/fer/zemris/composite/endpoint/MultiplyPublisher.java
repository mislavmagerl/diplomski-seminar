package hr.fer.zemris.composite.endpoint;

import hr.fer.zemris.composite.Connection;
import hr.fer.zemris.composite.service.MultiplyService;
import hr.fer.zemris.composite.service.MultiplyServiceImpl;

import javax.xml.ws.Endpoint;

public class MultiplyPublisher {
    public static void main(String[] args) {
        Endpoint.publish(
                Connection.generateAddress(
                        MultiplyService.HOST,
                        MultiplyServiceImpl.PORT,
                        MultiplyService.NAME
                ),
                new MultiplyServiceImpl()
        );
    }
}
