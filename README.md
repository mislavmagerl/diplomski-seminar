# Seminarski rad

Seminar je pisan u LaTeX-u i nalazi se u direktoriju `paper`. Za generiranje `PDF`-a koristiti naredbu `pdflatex seminar.tex`. 

Periodično ću generirati `PDF` i staviti ga u `root` projekta.

# Prezentacija

Prezentacija je pisana u PowerPointu i nalazi se u direktoriju `presentation`.

# Struktura koda

Repozitorij se sastoji od (za sada) 3 projekta: `ServiceCommon` koji sadrzi kod koji svi projekti dijele, `MatrixMultiplier` - servis koji mnozi matrice te `Client` koji koristi usluge servisa.

U buducnosti ce biti dodani drugi servisi.

Svi projekti su `Maven` projekti.
