\documentclass[times, utf8, seminar, numeric]{fer}

\usepackage{booktabs}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage{mathtools}
\usepackage{listings}
\usepackage{multirow}
\usepackage{tikz}
\usepackage{svg}
\usepackage{listings}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=left,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\renewcommand{\lstlistingname}{Isječak koda}% Listing -> Isječak koda

\begin{document}

\title{Okolina za ispitivanje pouzdanosti kompozicija servisa}
\author{Mislav Magerl}
\voditelj{prof. dr. sc. Siniša Srbljić}

\maketitle

\tableofcontents

\chapter{Uvod}

Arhitektura sustava se pojavom usluga u oblaku sve više mijenja. Sustavi se više ne izvode na jednom računalu koji obavlja sav posao, već se pojavljuju specijalizirani servisi koji obavljaju svaki svoju specifičnu zadaću. 

Također, u budućnosti će biti sve više uređaja koji će biti spojeni na Internet te komunicirati u mreži. Uz te uređaje često se veže pojam Internet stvari \engl{Internet of Things}. Zahtjevi tih uređaja bit će da nisu skupi te da rade računalno zahtjevne poslove. Smisao arhitekture zasnovane na uslugama \engl{Service Oriented Architecture} jest da se računalno zahtjevan posao obavlja na nekom za to predviđenom uređaju, tj. na serveru. 

Upravo zbog velikog broja uređaja koji koriste takve servise i zbog velike računalne složenosti programa koje ti servisi izvode potrebno je da se na neki način može izračunati njihova pouzdanost. 

U poglavlju \ref{ch:model} opisat će se model pouzdanosti koji se koristi za opis kompozitnih programa. U poglavlju \ref{ch:environment} bit će predložena implementacija sustava zasnovanog na servisima. U poglavlju \ref{ch:testing} bit će prikazani načini ispitivanja korelacije između modela i stvarnog sustava. Na poslijetku, u poglavlju \ref{ch:conclusion} bit će pokazani rezultati rada te plan budućeg istraživanja ove teme.


\chapter{Kompozitni programi i model pouzdanosti}
\label{ch:model}
U ovom poglavlju opisan je pojam kompozitnih programa i definiano je od čega se oni sastoje te kako se na temelju modela dobivaju podaci koji se mogu grupirati.

\section{Kompozitni programi}
Arhitektura orijentirana prema uslugama \engl{Service Oriented Architecture} jest arhitekturni stil koji pruža smjernice za razvijanje labavo povezanih distribuiranih sustava. Taj koncept se oslanja na samostalne servise postavljene na mreži koji se mogu koristiti pomoću dobro definiranih sučelja \citep{reliability}. Na temelju tih samostalnih servisa grade se kompozitni programi koji mogu na razne načine ovisiti o servisima. Primjer programa koji ovisi o servisima $S_1$ -- $S_6$ prikazan je na slici \ref{fig:img-kompozitni-program}. Prikazani program prvo slijedno ovisi o servisima $S_1$ i $S_2$, zatim paralelno ovisi o servisima $S_3$ te $S_4$ i $S_5$.  Nakon paralele opet ovisi o servisu $S_2$ i konačno se grana na jednu od dvije grane koje sadrže servise $S_1$ i $S_6$, odnosno samo servis $S_1$.

\begin{figure}[htb]
\centering
\includegraphics[width=12.5cm]{images/kompozicija.png}
\caption{Primjer ovisnosti kompozitnog programa o servisima}
\label{fig:img-kompozitni-program}
\end{figure}


\section{Model pouzdanosti}
Formalno, model pouzdanosti se definira kao usmjereni graf bez ciklusa \engl{Directed Acyclic Graph}. Graf se sastoji od konačnog broja čvorova koji predstavljaju slučajne varijable i bridova koji određuju ovisnosti između čvorova. Svaki graf ima nekoliko ulaznih slučajnih varijabli koji predstavljaju atomarne servise o kojima program ovisi te jednu izlaznu varijablu koja predstavlja pouzdanost kompozicije servisa. Između ulaznih i izlazne varijable postoji skriveni sloj čvorova koji oblikuju ovisnosti o ulaznim varijablama. 
 
Unutarnji čvorovi određuju tipove ovisnosti izlazne varijable o ulaznim varijablama. Primjerice, sekvencijalni čvor označava da se svi čvorovi roditelji moraju izvesti, dok paralelni čvor označava da se barem $k$ roditelja mora izvesti.

\begin{figure}[htb]
\centering
\includegraphics[width=12.5cm]{images/model.png}
\caption{Model kompozitnog programa}
\label{fig:img-model}
\end{figure}

Na slici \ref{fig:img-model} prikazan je općeniti model jednog programa koja se sastoji od ulaznih čvorova, skrivenih čvorova te izlaznog čvora -- samog programa.

\section{Vrste čvorova}
Model pouzdanosti gradi se kao vjerojatnosna mreža, odnosno kao usmjereni graf bez ciklusa. Graf se sastoji od šest vrsta čvorova koji su opisani u nastavku poglavlja.

\subsection{Servis}
Čvor servisa je slučajna varijabla koja predstavlja pouzdanost atomarnog servisa od kojih je građena kompozicija. Čvorovi servisa nemaju roditeljske čvorove jer po definiciji ne ovise o drugim komponentama. Pouzdanost čvora servisa je za potrebe modela unaprijed poznata, a u stvarnosti bi se mogla mjeriti empirijski. 

\subsection{Kompozicija}
Čvor kompozicije je izlazni čvor modela i predstavlja ukupnu pouzdanost kompozitnog programa. Kompozicija nema ni jedan čvor dijete i jedini je list u grafu. Pouzdanost čvora kompozicije računa se na isti način kao i za čvor slijeda, koji je opisan u sljedećem poglavlju. 


\subsection{Slijed}
Čvor slijeda \engl{Sequence node} označava da se čvorovi roditelji moraju izvesti slijedno. To znači da je pouznanost slijednog čvora umnožak pouzdanosti njegovih roditelja:

\begin{equation}
P(Seq)=\prod_{i=1}^{n}P(X_i)
\label{eq:sequence}
\end{equation}

Na slici \ref{fig:img-sequence} sa desne strane vidi se čvor slijeda (označen sa $Seq$) na kojeg su povezana dva čvora, dok je sa lijeve strane prikazan tijek izvođenja.

\begin{figure}[htb]
\centering
\includegraphics[width=5cm]{images/sequence-node.png}
\caption{Čvor slijeda}
\label{fig:img-sequence}
\end{figure}


\subsection{Grananje}
Čvor grananja \engl{Branch node} označava da će se izvesti točno jedan od čvorova roditelja. Kod čvora grananja za svakog je roditelja poznata vjerojatnost da će se taj roditelj izvesti. Također vrijedi:

\begin{equation}
\sum_{i=1}^{n}p_i=1
\label{eq:sum-p_i}
\end{equation}

Pouzdanost čvora grananja je suma umnožaka vjerojatnosti da će se neka grana izvesti i pouzdanosti te grane:
\begin{equation}
P(B)=\sum_{i=1}^{n}p_iP(X_i)
\label{eq:branch}
\end{equation}

Na slici \ref{fig:img-branch} sa desne strane vidi se čvor grananja na koji su spojene komponenta $S_3$ i sekvencijski čvor. Izvest će se točno jedan od ta dva čvora. Također, sa lijeve strane vidi se tijek izvođenja.

\begin{figure}[htb]
\centering
\includegraphics[width=7cm]{images/branch-node.png}
\caption{Čvor grananja}
\label{fig:img-branch}
\end{figure}

\subsection{Paralela}
\label{sec:paralel}
Čvor paralele \engl{Parallel node} označava da se barem $k$ grana mora izvesti. To znači da se sumiraki sve kombinacije u kojima je više od $k$ grana izvedeno.

Na slici \ref{fig:img-parallel} sa desne strane vidi se čvor paralele na koji su, kao i na čvor grananja, spojene komponenta $S_3$ i čvor slijeda. Pod pretpostavkom da je $k=1$, mora se izvesti samo sekvencijski čvor, samo komponenta $S_3$ ili oboje. Sa lijeve strane prikazan je tijek izvođenja. 

\begin{figure}[htb]
\centering
\includegraphics[width=7cm]{images/parallel-node.png}
\caption{Čvor paralele}
\label{fig:img-parallel}
\end{figure}

\subsection{Petlja}
Čvor petlje \engl{Loop node} ima točno jednog roditelja i označava da se ta grana mora izvesti točno $k$ puta. To znači da je pouzdanost tog čvora $k$-ta potencija pouzdanosti roditeljskog čvora:
\begin{equation}
P(L)=P(X_j)^k
\label{eq:loop}
\end{equation}

Na slici \ref{fig:img-loop} sa desne strane vidi se čvor petlje spojen na sekvencijski čvor. Sekvencijski čvor će se izvesti $k$ puta, što se prikazuje na tijeku izvođenja sa lijeve strane.

\begin{figure}[htb]
\centering
\includegraphics[width=6cm]{images/loop-node.png}
\caption{Čvor petlje}
\label{fig:img-loop}
\end{figure}

\chapter{Stvaranje okoline}
\label{ch:environment}
Kompozitni servisi se sastoje od nekoliko komponenti: 
\begin{itemize}
\item Atomarni servisi
\item Programi koji koriste atomarne servise
\item Sučelje za komunikaciju atomarnih servisa i kompozitnih programa
\end{itemize}

Za ovaj rad stvorena je implementacija okoline za kompozitne servise u programskom jeziku \textit{Java}. 
Svaki atomarni servis definira svoje sučelje kao jednu metodu s ulaznim i izlaznim parametrima. 
Za povezivanje servisa i klijenta (kompozitnog programa) koristi se tehnologija \textit{JAX-WS} \engl{Java API for XML Web Services}. 
Ta tehnologija omogućava prenošenje \textit{Java} objekata preko mreže. 
U pozadini se koristi \textit{JavaBeans} tehnologija.

\section{Implementacija okoline}
\label{ch:implementation}
Imlementacija okoline ostvarena je u jeziku \textit{Java}. 
Cijeli projekt se može podijeliti na tri dijela:

\begin{enumerate}
\item dijeljeni kod, \engl{Common},
\item $n$ implementacija servisa,
\item $m$ kompozitnih programa.
\end{enumerate}

Dijeljeni kod je jedan od najbitnijih dijelova projekta. 
U njemu su definirani svi modeli koji se prenose između servisa te sučelja svih atomarnih servisa. 

Svaki atomarni servis ostvaren je kao jedan projekt koji sadrži dijeljeni kod te implementaciju sučelja kojeg ostvaruje. 
Uz implementaciju sučelja servis sadrži i \textit{main} metodu koja pokreće servis na odabranom \textit{URL}-u. 

Kompozitni programi su ostvareni kao zasebni projekti koji zovu metode atomarnih servisa. 
Dakle, oni sami ne izvode kod, već se kod izvodi na atomarnom servisu koji rezultat onda šalje programu putem mreže.

U nastavku bit će opisana implementacija jednog servisa i jednog klijenta. Servis radi posao množenja matrice, a klijent zove taj servis sa dvije matrice i ispisuje dobiveni rezultat.

\subsection{Dijeljeni kod}
U dijeljenom kodu definirani su modeli koji se prenose između servisa. Jedini model u ovom jednostavnom primjeru je matrica. Modelira se razredom \texttt{Matrix} opisanog u isječku koda \ref{lst:matrix}.

\begin{lstlisting}[{caption=Razred koji modelira matricu.},label={lst:matrix}]
public class Matrix {

    private int rowsCount;
    private int colsCount;
    private double[][] elements;

    public Matrix() {}

    // getteri i setteri
}
\end{lstlisting}

U dijeljenom kodu također su definirani svi servisi. Oni se definiraju pomoću sučelja u Javi. U ovom primjeru postoji samo jedan servis koji množi dvije matrice. Sučelje je prikazano u isječku koda \ref{lst:multiply}.

\begin{lstlisting}[caption={Sučelje koje definira servis.},label={lst:multiply}]
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface MultiplyService {
    String NAME = "MultiplyService";
    Integer PORT = 9999;
    String HOST = "localhost";

    /**
     * Returns the result of multiplication of matrices m1 and m2.
     */
    @WebMethod
    Matrix multiply(Matrix m1, Matrix m2);
}
\end{lstlisting}

Anotacija \texttt{@WebService} označava da sučelje prestavlja web servis. Anotacija \texttt{@WebMethod} označava metodu koju servis nudi. Posljednje, anotacija \texttt{@SOAPBinding} određuje kako se WSDL prevodi u SOAP. Postoje dva načina, poziv udaljene prodedure, \engl{Remote Procedure Call (RPC)}, te stil dokumenta \engl{Document style}. Više o tome može se pročitati na \citep{SOAPBinding}.

Ovo sučelje također ima i svoje varijable koje označavaju ime servisa, IP adresu, odnosno ime poslužitelja te port na kojem se servis nalazi. Time se sve informacije o servisu nalaze u samoj definiciji servisa.

Posljednji razred, pod imenom \texttt{Connection} sadrži metode koje dohvaćaju vezu sa servisima. Kod tog razreda prikazan je u isječku koda \ref{lst:connection}.


\begin{lstlisting}[caption={Metode koje dohvaćaju vezu sa servisima},label={lst:connection}]
public class Connection {
    public static final String NAMESPACE_URI = "http://service.composite.zemris.fer.hr/";

    public static MultiplyService multiplyService() {
        Service service = getService(
                MultiplyService.HOST, 
                MultiplyService.PORT, 
                MultiplyService.NAME
        );
        return service.getPort(MultiplyService.class);
    }
    public static String generateAddress(String host, int port, String name){
        return "http://" + host + ":" + port + "/ws/" + name;
    }
    public static Service getService(String host, int port, String name){
        URL wsdlURL = getWsdlURL(host, port, name);
        QName qname = getQName(name);
        return Service.create(wsdlURL, qname);
    }
    private static URL getWsdlURL(String host, int port, String name){
        return new URL(generateAddress(host, port, name) + "?wsdl");
    }
    private static QName getQName(String serviceName){
        return new QName(NAMESPACE_URI, serviceName + "ImplService");
    }
}
\end{lstlisting}

U ovom razredu svaki servis ima jednu metodu koja se zove kao servis i dohvaća implementaciju tog servisa sa \texttt{URL}-a na kojem se servis nalazi. 


\subsection{Implementacija servisa}

Za implementaciju sučelja dovoljno je napisati kod za dva razreda. Prvi razred jest direktna implementacija sučelja koje servis nudi. To se na primjeru množenja matrice može vrlo jednostavno implementirati. Kod je prikazan u isječku \ref{lst:multiplyService}.

\begin{lstlisting}[caption={Implementacija sučelja MultiplyService},label={lst:multiplyService}]
@WebService(endpointInterface = "hr.fer.zemris.composite.service.MultiplyService")
public class MultiplyServiceImpl implements MultiplyService {
    public Matrix multiply(Matrix m1, Matrix m2) {
        if (m1.getColsCount() != m2.getRowsCount()) {
            throw new IllegalArgumentException("Broj redaka prve matrice mora biti broju stupaca druge matrice");
        }
        int rows = m1.getRowsCount();
        int cols = m2.getColsCount();
        int others = m1.getColsCount();
        Matrix matrix = new Matrix(rows, cols);
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                double value = 0;
                for (int k = 0; k < others; k++) {
                    value += m1.getElement(row, k) * m2.getElement(k, col);
                }
                matrix.set(row, col, value);
            }
        }
        return matrix;
    }
}
\end{lstlisting}

Moguće je primijetiti da implementacija sučelja ima anotaciju \texttt{@WebService} u kojoj je napisano puno ime sučelja koje ovaj razred implementira. To služi da bi se kod napisan u Javi mogao prevesti u \textit{SOAP} standard. 

Drugi razred potreban u implementaciji servisa jest razred koji sadrži \texttt{main} metodu koja pokreće servis. Taj razred prikazan je u isječku \ref{lst:MultiplyPublisher}.

\begin{lstlisting}[caption={Razred koji pokreće servis},label={lst:MultiplyPublisher}]
public class MultiplyPublisher {
    public static void main(String[] args) {
        Endpoint.publish(
                Connection.generateAddress(
                        MultiplyService.HOST,
                        MultiplyServiceImpl.PORT,
                        MultiplyService.NAME
                ),
                new MultiplyServiceImpl()
        );
    }
}
\end{lstlisting}

\subsection{Kompozitni program}

Kompozitni program zove metode servisa koji su definirani u dijeljenom kodu. U ovom jednostavnom primjeru program stvara jednu matricu i potencira ju na treću potencuiju uzastopnim množenjem matrice sa samom sobom. Može se primjetiti da je moguće napraviti samostalan servis koji potencira matricu na neku potenciju, a u pozadini zove servis za množenje matrica te koristi brzo potenciranje. 

Primjer ovog jednostavnog klijenta vidi se na isječku \ref{lst:Client}.

\begin{lstlisting}[caption={Program koji zove servis},label={lst:Client}]
public class Client {
    public static void main(String[] args) {
        MultiplyService multiplyService = Connection.multiplyService();
        
        Matrix m1= Matrix.parse("0 0 0 0.5 | 0.5 0 0.5 0.5 | 0 1 0 0 | 0.5 0 0.5 0");
        Matrix m2 = multiplyService.multiply(m1, m1);
        Matrix m3 = multiplyService.multiply(m2, m1);

        System.out.println(m3);
    }
}
\end{lstlisting}


\chapter{Ispitivanje okoline}
\label{ch:testing}

Pouzdanost kompozitnih programa može se računati na dva načina. Prvi je obaviti detaljno ispitivanje programa, što zahtjeva puno resursa. Moguće je i promašiti neke puteve izvođenja što može dovesti do krivih rezultata. Drugi način jest za neki program napraviti model pouzdanosti i njega evaluirati.

U ovom poglavlju napravit će se pregled metoda kojim bi se mogla pokazati korelacija između modela pouzdanosti (\citep{prediction}) i stvarne pouzdanosti programa. Ako se pokaže da su to dvoje u visokoj korelaciji mogu se znatno smanjiti resursi koji se koriste za testiranje kritičnih raspodijeljenih sustava.


\section{Ispitivanje okoline}

Za potrebe ranijih istraživanja \citep{finalPaper} stvoren je generator modela pouzdanosti koji nasumično, ali s obzirom na zadana ograničenja, stvara modele kompozitnih programa. Cilj ovog rada jest stvoriti okolinu koja će pokazati korelaciju između eksperimentalnih podataka i stvarnih podataka dobivnih mjerenjem nad stvarnim servisima.

Ispitivanje na stvarnim podacima može se izvesti na nekoliko načina. Predložena su dva načina. Prvi način jest stvaranje klijentskih kompozitnih programa te mjerenje pouzdanosti servisa. Drugi način jest automatizirano testiranje performansi korištenjem dostupnih alata za testiranje.

\subsection{Ispitivanje stvaranjem konkretnih programa}

Obrazac koji je postavljen u ovom radu je dovoljan da se stvore implementacije modela koje stvara generator. Čvorovi slijeda, grananja i petlje iz modela pouzdanosti jednostavno se programski mogu izvesti osnovnim konstruktima jezika \textit{Java}, a čvor paralele se može izvesti asinkronim pozivanjem servisa te čekanjem na završetak $k$ servisa (detaljnije se može pročitati u odjeljku \ref{sec:paralel}).

U daljnjem istraživanju svakako će se razmotriti ova opcija kao jedna od osnovnih u ispitivanju pouzdanosti kompozitnih programa. Potrebno je za neke modele generirati programe, odnosno za neke programe generirati modele i tražiti korelaciju između pouzdanosti dobivenih na ta dva načina.

\subsection{Ispitivanje korištenjem alata za testiranje}

Drugi način ispitivanja okoline jest korištenjem gotovih alata za testiranje opterećenja sustava. Jedan od alata koji upravo služe takvom testiranju jest \textit{Apache JMeter}.
Jedan od razloga zašto bi ovaj alat mogao biti dobar za testiranje predložene okoline jest taj što je sam alat pisan u Javi te ima podršku za \textit{SOAP} protokol. Također, postoje članci koji 	predlažu automatizirano testiranje internetskih servisa korištenjem tog alata \citep{JMeter}. 

Za ovaj pristup potrebno je osmisliti preslikavanje iz modela pouzdanosti u model koji jedan takav alat može testirati. Kada se napravi takvo preslikavanje onda je puno jednostavnije provesti testiranja korelacije koju tražimo. Naime, generator modela pouzdanosti može stvoriti proizvoljno velik broj modela koji se tada mogu dati na testiranje već navedenom alatu. Takvo ispitivanje se može u potpunosti automatizirati i provesti na jako velikom uzorku.


\chapter{Zaključak}
\label{ch:conclusion}
Korištenjem modela pouzdanosti kompozitnih programa moguće je znatno smanjiti resurse potrebne za ispitivanje pouzdanosti sustava. U ovom radu dan je predložak okoline za razvoj sustava kompozitnih programa. Ta okolina je teoretski dovoljno robusna da se u njoj mogu napraviti programi vrlo visoke složenosti.  Pokazano je da je moguće iz nekog programa napraviti odgovarajući model te da se za neki model može provesti testiranje.

Predloženi su načini na koje se može pokazati korelacija između pouzdanosti dobivene korištenjem modela pouzdanosti te stvarnim pouzdanostima tih programa. Generator modela pouzdanosti (opisan u \citep{finalPaper}) je dovoljno robusan za stvaranje dovoljnog broja modela na kojima će se u daljnjem radu ispitati ta korelacija.

U budućnosti će biti sve više primjena kompozitnih programa temeljenih na servisima na Internetu. Važnu ulogu u razvoju Interneta stvari i pojma softvera kao usluge upravo ima mjerenje pouzdanosti i raspoloživosti takvih sustava. 

\bibliography{literatura}
\bibliographystyle{fer}

\begin{sazetak}
U seminarskom radu opisat će se definicija modela kompozitnih servisa koji se sastoje od atomarnih servisa. Istražit će se stvaranje okoline za ispitivanje pouzdanosti kompozicije servisa. Testirat će se okolina za ispitivanje pouzdanosti kompozitnih servisa te će se proučiti daljnje primjene okoline na ispitivanje algoritama.

  \kljucnerijeci{kompozitni programi, servisi, pouzdanost servisa, SOAP}
\end{sazetak}

\end{document}
